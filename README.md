![Build Status](https://gitlab.com/snacss/sapper-gitlab-pages/badges/master/pipeline.svg)

---

Hello World!

Example [Sapper][project] website using GitLab Pages.

Learn more about [GitLab Pages](https://about.gitlab.com/product/pages)
and visit the [official documentation](https://docs.gitlab.com/ee/user/project/pages/).

---

[[_TOC_]]

## GitLab CI

The static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```yaml:title=.gitlab-ci.yml
image: "node:lts-alpine"

before_script:
  - npm install

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - node_modules/
    - .cache/
    - public/

pages:
  script:
    - mkdir public/ || true
    - npm run export -- --basepath $CI_PROJECT_NAME
    - cp -a __sapper__/export/$CI_PROJECT_NAME/* public/
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == "master"

```

The file sets up the needed environment to run Sapper and to build out the website to the `./public` directory.

**Note**: Sapper usually runs as SSR application, [sapper export][sapper-export] makes
it so that it can also run as static generated pages (_à la Gatsby_).
In order to do so, this project simply adds the needed `.gitlab-ci.yml` to the default sapper template.

To account for GitLab Pages default configuration, the `$CI_PROJECT_NAME` environment variable is assumed. Please check [how to set the base path](#set-the-base-path) for your project.
You can find it [here](https://sapper.svelte.dev/docs#Getting_started).

### Set the base path

As the site will be hosted under `<yourname.gitlab.io>/<examplerepository>/`, you will need to configure Sapper to use the base path as a prefix.
In the `./src/server.js`, add the path corresponding to the name of your repository as the first parameter to the method `use()`.

The example for the current (which is `https://gitlab.com/snacss/sapper-gitlab-pages`), your base path should be **sapper-gitlab-pages**.
See [the docs page Base URLs](https://sapper.svelte.dev/docs/#Base_URLs).

```js:title=./src/server.js
.use(
  'sapper-gitlab-pages',
  compression({ threshold: 0 }),
  sirv('static', { dev }),
  sapper.middleware()
)
```

### Build and deploy with GitLab CI

The CI platform uses Docker images/containers, so `image: "node:lts-alpine"` tells the CI to use the latest node image. `cache:` caches the `node_modules` folder in between builds,
so subsequent builds should be a lot faster as it doesn't have to reinstall all the dependencies required. `pages:` is the name of the CI stage.
You can have multiple stages, e.g. 'Test', 'Build', 'Deploy' etc. `script:` starts the next part of the CI stage, telling it to start running the below scripts inside the image selected.
`npm install` will install all dependencies, and start the static site build, respectively.

Add that configuration, and with the next master branch push, your site should have been built correctly. This can be checked by going to your repository on GitLab,
and selecting CI/CD in the sidebar. This will then show you a log of all jobs that have either succeeded or failed.
You can click on the failed status, and then select the job to get more information about why your build may have failed.

If all went well, you should now be able to access your site. It will be hosted under `gitlab.io`.
For example if you have a repository under your namespace, the url will be `yourname.gitlab.io/examplerepository`.

Visit the [GitLab Pages](https://gitlab.com/help/user/project/pages/getting_started_part_one.md) to learn how to setup custom domains and find out about advanced configurations.

## Building locally

This project uses [npm](https://npmjs.com). For more info visti the [about page](https://docs.npmjs.com/about-npm/).

First, install the dependencies:

```
npm install
```

The, run the local dev server:

```
npm run dev
```

Your website should be available at [http://localhost:3000/]

_Read more at Sapper's [documentation]._

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

[ci]: https://about.gitlab.com/gitlab-ci/
[sapper-export]: https://sapper.svelte.dev/docs#Exporting
[project]: https://sapper.svelte.dev/
[install]: https://vuepress.vuejs.org/guide/getting-started.html
[documentation]: https://sapper.svelte.dev/docs/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages

---

Made with ❤️ from the Team at snacss.co

Forked from https://gitlab.com/snacss/sapper-gitlab-pages